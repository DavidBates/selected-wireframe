bl_info = {
    "name": "Selected Wireframe",
    "description": "A tool to display wireframes on the selected meshes.",
    "author": "David Bates",
    "blender": (2, 7, 3),
    "location": "View 3D > Tool Shelf",
    "category": "3D View",
}

import bpy

class RegWire(bpy.types.Operator):
    """Selected Wireframe"""
    bl_idname = "object.regester_wire_on_selected"
    bl_label = "Wireframe"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.app.handlers.scene_update_pre.append(wire)

        return {'FINISHED'}

#---------------------

class UnregWire(bpy.types.Operator):
	"""Selected Wireframe"""
	bl_idname = "object.unregester_wire_on_selected"
	bl_label = "Wireframe"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		for list in selected:
			obj = list[0]
			obj.show_wire = list[1]
			obj.show_all_edges = list[2]
		bpy.app.handlers.scene_update_pre.remove(wire)

		return {'FINISHED'}

#---------------------
selected = []

def wire(scene):
	global selected
	
	for list in selected:
		obj = list[0]
		obj.show_wire = list[1]
		obj.show_all_edges = list[2]

	selected = []

	for obj in scene.objects:
		if obj.select:
			selected.append([
				obj,
				obj.show_wire,
				obj.show_all_edges
			])
			
			obj.show_wire = True
			obj.show_all_edges = True


	
class ToolShelfMenu(bpy.types.Panel):
    """enabile and disabile wireframe on the select objects"""
    bl_category = "Tools"
    bl_label = "Slected Wireframe"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.label(text="Wireframe on selected")

        split = layout.split()
        col = split.column(align=True)

        if wire.__name__ in [hand.__name__ for hand in bpy.app.handlers.scene_update_pre]:
            col.operator("object.unregester_wire_on_selected", text="Hide Wire", icon='RESTRICT_VIEW_ON')
        else:
            col.operator("object.regester_wire_on_selected", text="Show Wire", icon='RESTRICT_VIEW_OFF')

def register():
    bpy.utils.register_class(ToolShelfMenu)

    bpy.utils.register_class(RegWire)
    bpy.utils.register_class(UnregWire)

def unregister():
    bpy.utils.unregister_class(ToolShelfMenu)

    bpy.utils.unregister_class(RegWire)
    bpy.utils.unregister_class(UnregWire)

if __name__ == "__main__":
    register()